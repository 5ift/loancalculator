import 'package:flutter/cupertino.dart';
import 'package:loancalculation/models/loanClass.dart';
import 'dart:math';

class LoanCalculatorProvider extends ChangeNotifier {

  Loan loan;

  void addLoan(double interestRate, double loanAmount, int loanTermInMonth, double paidAmount) {
    Loan tempLoan = Loan(interestRate: interestRate, loanAmount: loanAmount - paidAmount, loanTermInMonth: loanTermInMonth, paidAmount: paidAmount);
    this.loan = tempLoan;
  }

  void calculateMonthlyPayment() {
    
    double interestPerMonth = loan.interestRate / 1200;
    double interestPowerMonth = pow((1 + interestPerMonth), loan.loanTermInMonth);
    double numerator = loan.loanAmount * interestPerMonth * interestPowerMonth;
    double denominator = interestPowerMonth - 1;
    
    loan.paymentEveryMonth = numerator / denominator;

    print('${loan.paymentEveryMonth}');
    notifyListeners();
  }

  void calculateTotalPayment() {
    loan.totalPayment = loan.paymentEveryMonth * loan.loanTermInMonth;

    print('${loan.totalPayment}');
    notifyListeners();
  }

  void calculateTotalInterest() {
    loan.totalInterest = loan.totalPayment - loan.loanAmount;

    print('${loan.totalInterest}');
    notifyListeners();
  }

}
