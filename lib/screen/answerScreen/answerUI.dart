import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loancalculation/Provider/loanCalculatorProvider.dart';
import 'package:provider/provider.dart';

class AnswerUI extends StatelessWidget {
  const AnswerUI({Key key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    final loan =
        Provider.of<LoanCalculatorProvider>(context, listen: false).loan;

    return Scaffold(
      body: Container(
        decoration: BoxDecoration(
            gradient: LinearGradient(colors: [
          Color(0xffD16BA5),
          Color(0xffF86A8E7),
          Color(0xff5FFBF1)
        ], begin: Alignment.bottomLeft, end: Alignment.topRight)),
        child: Center(
          child: Stack(
            children: <Widget>[
              Container(
                padding: EdgeInsets.symmetric(horizontal: 10.0),
                height: 380,
                width: 340,
                decoration: BoxDecoration(
                    color: Colors.white,
                    borderRadius: BorderRadius.circular(20)),
                child: Column(
                  mainAxisAlignment: MainAxisAlignment.center,
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    SizedBox(
                      height: 40,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            'Payment Every Months:',
                            style: TextStyle(
                                color: Colors.grey[600], fontSize: 23.0),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            '\$ ' + loan.paymentEveryMonth.toStringAsFixed(2),
                            style:
                                TextStyle(color: Colors.black, fontSize: 17.0),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            'Total Payment:',
                            style: TextStyle(
                                color: Colors.grey[600], fontSize: 23.0),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            '\$ ' + loan.totalPayment.toStringAsFixed(2),
                            style:
                                TextStyle(color: Colors.black, fontSize: 17.0),
                          ),
                        ),
                      ],
                    ),
                    SizedBox(
                      height: 30,
                    ),
                    Column(
                      crossAxisAlignment: CrossAxisAlignment.start,
                      children: [
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            'Total Interest:',
                            style: TextStyle(
                                color: Colors.grey[600], fontSize: 23.0),
                          ),
                        ),
                        SizedBox(
                          height: 5.0,
                        ),
                        Container(
                          padding: EdgeInsets.symmetric(horizontal: 20),
                          child: Text(
                            '\$ ' + loan.totalInterest.toStringAsFixed(2),
                            style:
                                TextStyle(color: Colors.black, fontSize: 17.0),
                          ),
                        ),
                      ],
                    ),
                  ],
                ),
              ),
              Positioned(
                top: -70,
                right: 30,
                child: Container(
                  height: 150,
                  width: 150,
                  decoration: BoxDecoration(
                      gradient: LinearGradient(
                          colors: [Color(0xffD72E82), Color(0xffF5A52E)],
                          begin: Alignment.topRight,
                          end: Alignment.bottomLeft),
                      // color: Colors.white,
                      border: Border.all(color: Colors.white, width: 8),
                      borderRadius: BorderRadius.circular(100)),
                  child: Icon(
                    Icons.attach_money,
                    color: Colors.white,
                    size: 70.0,
                  ),
                ),
              ),
              Positioned(
                top: 15,
                left: 10,
                child: Container(
                  child: GestureDetector(
                    onTap: () {
                      Navigator.pop(context);
                    },
                    child: Icon(
                      Icons.arrow_back,
                      color: Color(0xffF86A8E7),
                      size: 30,
                    ),
                  ),
                ),
              ),
            ],
            overflow: Overflow.visible,
          ),
        ),
      ),
    );
  }
}
