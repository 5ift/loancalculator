import 'package:flutter/cupertino.dart';
import 'package:loancalculation/Provider/loanCalculatorProvider.dart';
import 'package:loancalculation/screen/loanInputScreen/loanInputUI.dart';
import 'package:provider/provider.dart';

class LoanInputScreen extends StatefulWidget {
  @override
  _LoanInputScreenState createState() => _LoanInputScreenState();
}

class _LoanInputScreenState extends State<LoanInputScreen> {

  TextEditingController loanamountController = new TextEditingController();
  TextEditingController loantermController = new TextEditingController();
  TextEditingController interestrateController = new TextEditingController();
  TextEditingController paidamountContoller = new TextEditingController();

  void onSubmitButtonPressed() {
    // initiate the provider to call the functions inside there
    LoanCalculatorProvider loanCalculatorProvider = Provider.of<LoanCalculatorProvider>(context, listen: false);
    // create a loan object in the provider
    loanCalculatorProvider.addLoan(
      double.parse(interestrateController.text),
      double.parse(loanamountController.text),
      int.parse(loantermController.text),
      double.parse(paidamountContoller.text),
    );
    // call the calculation functions
    loanCalculatorProvider.calculateMonthlyPayment();
    loanCalculatorProvider.calculateTotalPayment();
    loanCalculatorProvider.calculateTotalInterest();
  }

  @override
  Widget build(BuildContext context) {
    return LoanInputUI(
      loanamountController: loanamountController,
      loantermController: loantermController,
      interestrateController: interestrateController,
      paidamountContoller: paidamountContoller,
      onSubmitButtonPressed: onSubmitButtonPressed,
    );
  }
}