import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:loancalculation/screen/answerScreen/answerUI.dart';
import 'package:lottie/lottie.dart';

class LoanInputUI extends StatelessWidget {
  const LoanInputUI({
    this.loanamountController,
    this.loantermController,
    this.interestrateController,
    this.paidamountContoller,
    this.onSubmitButtonPressed,
  });

  final TextEditingController loanamountController;
  final TextEditingController loantermController;
  final TextEditingController interestrateController;
  final TextEditingController paidamountContoller;
  final Function onSubmitButtonPressed;

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      body: SingleChildScrollView(
        child: Container(
          height: MediaQuery.of(context).size.height * 1.2,
          decoration: BoxDecoration(
              gradient: LinearGradient(colors: [
            Color(0xffD16BA5),
            Color(0xffF86A8E7),
            Color(0xff5FFBF1)
          ], begin: Alignment.bottomLeft, end: Alignment.topRight)),
          child: Center(
            child: Column(
              mainAxisAlignment: MainAxisAlignment.start,
              children: <Widget>[
                Container(
                  margin: EdgeInsets.only(top: 200.0),
                  child: Stack(
                    children: <Widget>[
                      Container(
                        height: 400,
                        width: 320,
                        decoration: BoxDecoration(
                            color: Colors.white,
                            borderRadius: BorderRadius.circular(20)),
                        child: Column(
                          mainAxisAlignment: MainAxisAlignment.center,
                          children: [
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText: 'Loan Amount',
                                  labelStyle: TextStyle(fontSize: 20),
                                ),
                                controller: loanamountController,
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText: 'Loan Term (month)',
                                  labelStyle: TextStyle(fontSize: 20),
                                ),
                                controller: loantermController,
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText: 'Interest Rate %',
                                  labelStyle: TextStyle(fontSize: 20),
                                ),
                                controller: interestrateController,
                              ),
                            ),
                            SizedBox(
                              height: 15,
                            ),
                            Container(
                              padding: EdgeInsets.symmetric(horizontal: 30.0),
                              child: TextField(
                                keyboardType: TextInputType.number,
                                decoration: InputDecoration(
                                  labelText: 'Paid Amount',
                                  labelStyle: TextStyle(fontSize: 20),
                                ),
                                controller: paidamountContoller,
                              ),
                            ),
                          ],
                        ),
                      ),
                      Positioned(
                        top: -175,
                        child: Container(
                          height: 75,
                          width: 75,
                          decoration: BoxDecoration(
                            color: Colors.orange,
                            borderRadius: BorderRadius.circular(100),
                          ),
                          child: IconButton(
                              icon: Icon(Icons.check),
                              iconSize: 30,
                              color: Colors.white,
                              onPressed: () {
                                onSubmitButtonPressed();
                                Navigator.of(context).push(PageRouteBuilder(
                                  pageBuilder: (context, animation,
                                          secondaryAnimation) =>
                                      AnswerUI(),
                                  transitionsBuilder: (context, animation,
                                      secondaryAnimation, child) {
                                    var begin = Offset(0.0, 1.0);
                                    var end = Offset.zero;
                                    var curve = Curves.ease;

                                    var tween = Tween(begin: begin, end: end)
                                        .chain(CurveTween(curve: curve));

                                    return SlideTransition(
                                      position: animation.drive(tween),
                                      child: child,
                                    );
                                  },
                                ));
                              }),
                          margin: EdgeInsets.only(top: 40.0),
                          child: Text('LOAN',
                              style: TextStyle(
                                  fontFamily: 'Poppins',
                                  fontWeight: FontWeight.w800,
                                  fontSize: 40,
                                  color: Colors.white)),
                        ),
                      ),
                      Positioned(
                        top: -127,
                        child: Container(
                          margin: EdgeInsets.only(top: 40.0),
                          child: Text('Calculator',
                              style: TextStyle(
                                  fontSize: 35,
                                  fontWeight: FontWeight.w100,
                                  color: Colors.white)),
                        ),
                      ),
                      Positioned(
                        top: -150,
                        right: -30,
                        child: Container(
                          height: 275,
                          child: Lottie.asset('assets/lotties/saving.json'),
                        ),
                      ),
                      Positioned(
                        bottom: -35,
                        right: 30.0,
                        child: GestureDetector(
                          behavior: HitTestBehavior.translucent,
                          child: Container(
                            height: 75,
                            width: 75,
                            decoration: BoxDecoration(
                              color: Color(0xff86A8E7),
                              borderRadius: BorderRadius.circular(100),
                            ),
                            child: IconButton(
                                icon: Icon(Icons.check),
                                iconSize: 30,
                                color: Colors.white,
                                onPressed: () {
                                  onSubmitButtonPressed();
                                  Navigator.push(
                                    context,
                                    MaterialPageRoute(
                                      builder: (context) => AnswerUI(),
                                    ),
                                  );
                                }),
                          ),
                        ),
                      ),
                    ],
                    overflow: Overflow.visible,
                  ),
                ),
                SizedBox(
                  height: 60,
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }
}
