import 'package:flutter/material.dart';
import 'package:loancalculation/Provider/loanCalculatorProvider.dart';
import 'package:loancalculation/screen/loanInputScreen/loanInputScreen.dart';
import 'package:provider/provider.dart';

void main() {
  runApp(ChangeNotifierProvider(
      create: (_) => LoanCalculatorProvider(), child: MyApp()));
}

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
        theme: ThemeData(fontFamily: 'PulpDisplay'),
        title: 'Flutter Demo',
        debugShowCheckedModeBanner: false,
        home: AnimatedSplashScreen(
            duration: 3000,
            splash: Image.network(
                "https://thebottomline.as.ucsb.edu/wp-content/uploads/2011/01/MoneySavingTips.jpg"),
            nextScreen: LoanInputScreen(),
            splashTransition: SplashTransition.fadeTransition,
            backgroundColor: Colors.white));
  }
}
//
