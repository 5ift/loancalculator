class Loan {
  double totalPayment,
      interestRate,
      loanAmount,
      paidAmount,
      paymentEveryMonth,
      totalInterest;
  int loanTermInMonth; //in month

  Loan({
    double interestRate,
    double loanAmount,
    double paidAmount,
    int loanTermInMonth
  }) {
    this.interestRate = interestRate;
    this.loanAmount = loanAmount;
    this.paidAmount = paidAmount;
    this.loanTermInMonth = loanTermInMonth;
  }
}
